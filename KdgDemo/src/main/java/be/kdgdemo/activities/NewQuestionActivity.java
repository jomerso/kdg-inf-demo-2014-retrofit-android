package be.kdgdemo.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import be.kdgdemo.R;
import be.kdgdemo.model.Question;
import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

import static android.view.inputmethod.EditorInfo.IME_ACTION_SEND;

/**
 * @author Jo Somers
 */
public class NewQuestionActivity extends BaseActivity implements Callback<Question> {

    private static String TAG = NewQuestionActivity.class.getSimpleName();

    @InjectView(R.id.question)
    EditText questionInputEditText;

    @InjectView(R.id.submitQuestion)
    Button submitQuestionButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_questions_new);

        ButterKnife.inject(this);

        if (getActionBar() != null) {
            getActionBar().setHomeButtonEnabled(true);
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }

        initEditorActionListener();
    }

    @Override
    public void success(
            final Question question,
            final Response response) {

        finish();
    }

    @Override
    public void failure(final RetrofitError error) {
        questionInputEditText.setEnabled(true);
        submitQuestionButton.setEnabled(true);

        getVisualisation().showError(getString(R.string.tFailure));

        Log.e(TAG, error.toString());
    }

    @OnClick(R.id.submitQuestion)
    public void onQuestionSubmitClicked() {
        submitQuestion();
    }

    private void initEditorActionListener() {
        questionInputEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == IME_ACTION_SEND) {
                    submitQuestion();
                    handled = true;
                }

                return handled;
            }
        });
    }

    private void submitQuestion() {
        questionInputEditText.setEnabled(false);
        submitQuestionButton.setEnabled(false);

        final String questionText =
                questionInputEditText != null
                        && questionInputEditText.getText() != null ? questionInputEditText.getText().toString() : "";

        final Question question = new Question();
        question.setQuestion(questionText);

        getService().submitQuestion(question, this);
    }

}
