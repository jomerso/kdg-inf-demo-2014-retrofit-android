package be.kdgdemo.model;

import org.joda.time.DateTime;

/**
 * @author Jo Somers
 */
public class Answer {

    private String questionId;

    private String answer;

    private DateTime modified;

    /**
     * Mandatory empty constructor
     */
    public Answer() {
    }

    public String getAnswer() {
        return answer;
    }

    public DateTime getModified() {
        return modified;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public void setQuestionId(String questionId) {
        this.questionId = questionId;
    }
}
